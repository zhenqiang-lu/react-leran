import React from 'react';
import './App.css';
import Father from './components/Father'
import { Button } from "antd";
import 'antd/dist/reset.css';
import "./index.css";
import { BrowserRouter as Router, Route,useHistory,Switch } from "react-router-dom";
import Home from "./pages/home/index"
import Table from "./pages/table/index"
import Hooks from "./pages/hooks/modifyIndex"
import TableNew from "./pages/tableNew/index"
// import Hooks from "./pages/hooks/delIndex"
// import Hooks from "./pages/hooks/addIndex"

const App = ()=> {
  return (
    <Router>
      <Switch>
        <Route exact path={'/home'} component={Home}></Route>
        <Route exact path={'/table'} component={Table}></Route>
        <Route exact path={'/hooks'} component={Hooks}></Route>
        <Route exact path={'/'} component={TableNew}></Route>
      </Switch>
    </Router>
  );
}


export default App;
