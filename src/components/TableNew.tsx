import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Table,  Space } from 'antd';

interface RowData {
  name: string;
  age: string;
  id: number;
}

const App: React.FC = () => {
    useEffect(() => {
      // 有点类似于watch 组件初始化时候执行
     getList();
    }, [])
    const [list, setList] = useState<RowData[]>([])
    const [loading, setLoading] = useState(false)
    const columns = useMemo(() => {
      return [
        {
          title: '姓名',
          dataIndex: 'name',
        },
        {
          title: '年龄',
          dataIndex: 'age',
        },
        {
          title: '描述',
          dataIndex: 'desc',
        },
        {
          title: '操作',
          render: (_: any, record: any) => {
            return <Space>
            <a>编辑</a>
            <a onClick={() => onDel(record)}>删除</a>
            <a onClick={() => onAdd()}>新增</a>
            </Space>
          }
        }
      ]
    }, [list])

    // 模拟接口请求
    const getList = useCallback(async () => {
      setLoading(true)
      const result: RowData[] = await getListAjax()
      setLoading(false)
      setList(result);
    }, [])

    // 模拟 axios 可以不用管这个函数
    const getListAjax = useCallback(() => {
      return new Promise<RowData[]>((resolve, reject) => {
        setTimeout(() => {
         const res = new Array(100).fill(0).map((i, index) => ({ name: `name${index}`, age: `age${index}`, id: index }))
         resolve(res);
        }, 2000);
      })
    }, [])

    const showList = useMemo(() => {
      // 有点类似于计算属性
      return list.map(item => {
        return {
          ...item,
          desc: `这是第${item.id}行`
        }
      })
    }, [list])

    // add
    const onAdd = useCallback(async ()=>{
      const res = await addAjax({name:`name${list.length}`,age:`age${list.length}`,id:Number(`${list.length}`)})
      setList(res)
    },[list])

    // del
    const onDel = useCallback(async (record:RowData) => {
      // 建议写法
      // const _list = [...list]
      // _list.splice(record.id,1)


      // 不建议直接操作list
      //  list.splice(id,1)
      //  setList([...list])
      const res = await delAjax(record.id)
      setList(res)
    },[list])

    // add ajax
    const addAjax = useCallback((item:RowData) => {
      let _list = [...list]
      return new Promise<RowData[]>((resolve,reject) => {
         _list.unshift(item)
        resolve(_list)
      })
    },[list])

    // del ajax
    const delAjax = useCallback((id:number) => {
      let _list = [...list]
      return new Promise<RowData[]>((resolve, reject) => {
         const res = _list.filter((i) => (i.id !== id))
         resolve(res);
      })
    },[list])

    return (
        <Table
          rowKey={'id'}
          loading={loading}
          bordered
          dataSource={showList}
          columns={columns}
        />
    );
  };

  export default App;
