import React,{useState} from 'react'

const Child = (prop:{childData:number;getValue:Function}) => {
    const [count,setCount] = useState(1)
    const addValue = () => {
        setCount(count + 1)
    }
    const [value,setValue] = useState(0)
    const sendValue = () => {
        setValue(value+1)
        prop.getValue(value+1)
    }
    const val = prop.childData
        return(
            <div>
                我是子组件接受父组件的传值是：{val}
                <div>{count}</div>
                <button onClick={addValue}>点击+1</button>
                <button onClick={sendValue}>点击传值给父组件</button>
            </div>
        )
}

export default Child