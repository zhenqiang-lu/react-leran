import React,{useState} from "react";
import Child from './Child'

const Father = () => {
    const [childData] = useState(0)
    const [val,setVal] = useState(0)
    const getVal = (val:number)=> {
        console.log(val)
        setVal(val)
    }
    return (
        <div>
            <div>我是父组件接收到子组件的值为{val}</div>
           <Child childData={childData} getValue={getVal}></Child>
        </div>
    )
}
 
export default Father