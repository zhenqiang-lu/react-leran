import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Table,  Space } from 'antd';
import { getMaxListeners } from 'process';

interface RowData {
  name: string;
  age: string;
  id: number;
}

const App: React.FC = () => {
    useEffect(() => {
      // 有点类似于watch 组件初始化时候执行
     getList();
    }, [])

    const columns = useMemo(() => {
      return [
        {
          title: '姓名',
          dataIndex: 'name',
        },
        {
          title: '年龄',
          dataIndex: 'age',
        },
        {
          title: '描述',
          dataIndex: 'desc',
        },
        {
          title: '操作',
          render: () => {
            return <Space>
            <a>编辑</a>
            <a>删除</a>
            </Space>
          }
        }
      ]
    }, [])

    const [loading, setLoading] = useState(false)
    const [list, setList] = useState<RowData[]>([])
    // 模拟接口请求
    const getList = useCallback(async () => {
      setLoading(true)
      const result: RowData[] = await getListAjax()
      setLoading(false)
      setList(result);
    }, [])

    // 模拟 axios 可以不用管这个函数
    const getListAjax = useCallback(() => {
      return new Promise<RowData[]>((resolve, reject) => {
        setTimeout(() => {
         const res = new Array(100).fill(0).map((i, index) => ({ name: `name${index}`, age: `age${index}`, id: index }))
         resolve(res);
        }, 2000);
      })
    }, [])

    const showList = useMemo(() => {
      // 有点类似于计算属性
      return list.map(item => {
        return {
          ...item,
          desc: `这是第${item.id}行`
        }
      })
    }, [list])

    return (
        <Table
          rowKey={'id'}
          loading={loading}
          bordered
          dataSource={showList}
          columns={columns}
        />
    );
  };

  export default App;
