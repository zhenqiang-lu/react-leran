import React from 'react'
import { BrowserRouter as Router, Route,useHistory,Switch } from "react-router-dom";
import { Button } from "antd";
// import Table from '../../components/Table'

const Home = () => {
    const history = useHistory();
  console.log(history);
  const goToTable = () => {
    history.push('/table')
  }
  return (
    <Button onClick={goToTable} type="primary">goToTable</Button>
  )
}

export default Home