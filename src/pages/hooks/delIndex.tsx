import React, { memo, useCallback, useEffect, useMemo, useState } from "react";

const Hooks = memo(() => {
  const [list, setList] = useState([
    { name: 'a' },{ name: 'b' },{ name: 'c' }
  ])

  const showList = useMemo(() => {
    console.log(list,'list')
    return list;
  }, [list])

  const delItem = useCallback(() => {
    let _list = [...list];
    _list = _list.slice(0,-1)
    setList(_list);
    console.log(_list,'_list')
  }, [list])

  // useEffect(() => {
  //   console.log('list 变化了', list);
  // }, [list])

  // useEffect(() => {
  //   console.log('showList 变化了', showList);
  // }, [showList])

  return (
    <div>
      <ul>
        {
          showList.map((item, index) => <li key={index}>{item.name}</li>)
        }
      </ul>
      <button onClick={() => delItem()}>delete</button>
    </div>
  )
})

export default Hooks;
