// import React, { memo, useCallback, useEffect, useMemo, useState } from "react";

// const Hooks = memo(() => {
//   const [list, setList] = useState([
//     { name: 'a',editing:false },{ name: 'b',editing:false },{ name: 'c',editing:false }
//   ])
//   const [oldItem, setOldItem] = useState('')

//   const showList = useMemo(() => {
//     return list;
//   }, [list])

//   const saveItem = useCallback((item:any,index:any) => {
//     let _list = [...list]
//     _list[index].name = oldItem
//     _list[index].editing = false
//     setList(_list)
//   }, [oldItem])

//   const onChange = useCallback((item:any,index:any) => {
//     let _oldItem = oldItem
//     _oldItem = item
//     setOldItem(item)
//     console.log(oldItem)
//   },[oldItem])

//   const editItem = useCallback((item:any,index:any) => {
//     let _list = [...list]
//     _list.map(item=>{
//       item.editing = false
//     })
//     _list[index].editing = true
//     setList(_list);
//     setOldItem(item.name)
//   },[list])

//   const addItem = useCallback(() => {
//     const _list = [...list];
//    _list.map(item=>{
//     item.editing = false
//    })
//     _list.push({ name: 'd', editing:false })
//     setList(_list);
//   }, [list])

//   const cancelItem = useCallback((index:any) => {
//     const _list = [...list];
//     console.log(oldItem,'oldItem')
//     _list.map(item=>{
//       item.editing = false
//     })
//     setList(_list)
//   },[list])

//   return (
//     <div>
//       <ul>
//         {
//           showList.map((item, index) => (<li key={index}>
//             {
//                item.editing ?
//               <input onChange={(e)=> onChange(e.target.value,index)} value={oldItem}  />
//               :
//               <span>{item.name}</span>
//             }
//              <button onClick={() => editItem(item,index)}>edit</button>
//              <button onClick={() => saveItem(item,index)}>save</button>
//              <button onClick={() => cancelItem(index)}>cancel</button>
//           </li>))
//         }
//       </ul>
//       <button onClick={() => addItem()}>add</button>
//     </div>
//   )
// })

// export default Hooks;



import React, { memo, useCallback, useEffect, useMemo, useState } from "react";

interface Edit {
  name:string
  editing:boolean
}


const Hooks = memo(() => {
  const [list, setList] = useState([
    { name: 'a', editing: false }, { name: 'b', editing: false }, { name: 'c', editing: false }
  ])
  const [oldItem, setOldItem] = useState({ name: '', editing: false })

  const showList = useMemo(() => {
    return list;
  }, [list])

  // save
  const saveItem = useCallback((index: number) => {
    let _list = [...list]
    _list[index].name = oldItem.name
    _list[index].editing = false
    setList(_list)
  }, [oldItem,list])

  // onChange
  const onChange = useCallback((item: string) => {
    let _oldItem = oldItem
    _oldItem = { name: item, editing: true }
    setOldItem(_oldItem)
    console.log(oldItem)
  }, [oldItem])

  // edit
  const editItem = useCallback((item: Edit, index: number) => {
    let _list = [...list]
    _list.map(item => {
      item.editing = false
    })
    _list[index].editing = true
    setList(_list);
    setOldItem({ name: item.name, editing: true })
  }, [list])

  // add
  const addItem = useCallback(() => {
    const _list = [...list];
    _list.push({ name: 'd', editing: false })
    setList(_list);
  }, [list])

  // cancel
  const cancelItem = useCallback(() => {
    const _list = [...list];
    console.log(oldItem, 'oldItem')
    _list.map(item => {
      item.editing = false
    })
    setList(_list)
  }, [list])

  return (
    <div>
      <ul>
        {
          showList.map((item, index) => (<li key={index}>
            {
              item.editing ?
                <input onChange={(e) => onChange(e.target.value)} value={oldItem.name} />
                :
                <span>{item.name}</span>
            }
            <button onClick={() => editItem(item, index)}>edit</button>
            <button onClick={() => saveItem(index)}>save</button>
            <button onClick={() => cancelItem()}>cancel</button>
          </li>))
        }
      </ul>
      <button onClick={() => addItem()}>add</button>
    </div>
  )
})

export default Hooks;
