import React, { memo, useCallback, useEffect, useMemo, useState } from "react";

const Hooks = memo(() => {
  const [list, setList] = useState([
    { name: 'a' }
  ])

  const showList = useMemo(() => {
    console.log(list,'list')
    return list;
  }, [list])

  const addItem = useCallback(() => {
    const _list = [...list];
    _list.push({ name: 'b' })
    setList(_list);
    console.log(_list,'_list')
  }, [list])

  useEffect(() => {
    console.log('list 变化了', list);
  }, [list])

  useEffect(() => {
    console.log('showList 变化了', showList);
  }, [showList])

  return (
    <div>
      <ul>
        {
          showList.map((item, index) => <li key={index}>{item.name}</li>)
        }
      </ul>
      <button onClick={() => addItem()}>add</button>
    </div>
  )
})

export default Hooks;
